#1 Multiples of 3 and 5
(0...1000).select { |e| e%3==0 || e%5==0 }.inject(:+)

#2 Even Fibonacci numbers
a = [1, 2]

while a[-2] + a[-1] < 4000000
  a << a[-2] + a[-1]
end

a.reduce(0) { |sum, e| e.even? ? sum += e : sum }

#3 Largest prime factor

600851475143.prime_division

#4 Largest palindrome product

def palindrome?(str)
  str == str.reverse
end

(100..999).map do |x|
  res = (x..999).select { |e| palindrome?((x * e).to_s) }
  [x*res.max, x, res.max] if res.any?
end.compact.sort

# 5 Smallest multiple

(1..300000000).detect { |e| e%11==0 and e%12==0 and e%13==0 and e%14==0 and e%15==0 and e%16==0 and e%17 ==0 and e%18==0 and e%19==0 and e%20==0 }

answer = 2**4 * 3**2 * 5 * 7 * 11 * 13 * 17 * 19

#6 Sum square difference

sum_sq = (1..100).reduce(0) { |sum, x| sum += x * x }
sg_sum = (1..100).reduce(0) { |sum, x| sum + x }**2
sg_sum > sum_sq ? sg_sum - sum_sq : sum_sq - sg_sum

#6 attempts to improve

sum_sq, sg_sum = (1..100).reduce(0, 0) { |sum1, sum2, x| sum1 += x, sum2 += x*x }

sum_sq, sg_sum = 0, 0
(1..100).each { |e| sum_sq+=e; sg_sum+=e*e }

#7 10001st prime

Prime.first 10001

#8 Largest product in a sersum_sq = (1..100).reduce(0) { |sum, x| sum += x*x }ies

str.each_char.each_cons(13).map { |e| p e.reduce(1) { |pr, n| pr *= n.to_i } }.max

#10 Summation of primes

Prime.take_while { |p| p < 2000000 }.reduce(:+)
